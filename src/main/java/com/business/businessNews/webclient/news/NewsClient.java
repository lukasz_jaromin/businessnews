package com.business.businessNews.webclient.news;

import com.business.businessNews.dto.NewsDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@RequiredArgsConstructor
public class NewsClient {

    private static final String NEWSAPI_URL = "https://newsapi.org/v2/";
    private static final String API_KEY = "ae9e87d75daf4868941d74acd73bbbf6";
    private final RestTemplate restTemplate;

    public NewsDto getCategoryforCountry(String category, String country) {
        return restTemplate.getForObject(NEWSAPI_URL + "top-headlines?category={category}&country={country}&apiKey={apiKey}",
                NewsDto.class, category, country, API_KEY);
    }
}
