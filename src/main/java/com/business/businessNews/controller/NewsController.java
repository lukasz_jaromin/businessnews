package com.business.businessNews.controller;

import com.business.businessNews.dto.NewsDto;
import com.business.businessNews.service.NewsService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class NewsController {

    private final NewsService newsService;

    @GetMapping("/news")
    public NewsDto getNews() {
        return newsService.getNews();
    }

}
