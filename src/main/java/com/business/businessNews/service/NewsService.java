package com.business.businessNews.service;

import com.business.businessNews.dto.NewsArticlesDto;
import com.business.businessNews.dto.NewsDto;
import com.business.businessNews.webclient.news.NewsClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class NewsService {

    private static final String FILE_PATH = "src/main/resources/news.txt";
    private static final String CATEGORY = "business";
    private static final String COUNTRY = "pl";
    private final NewsClient newsClient;

    public NewsDto getNews() {
        final NewsDto newsDto = newsClient.getCategoryforCountry(CATEGORY, COUNTRY);
        writingToTheFile(newsDto);
        return newsDto;
    }

    private void writingToTheFile(NewsDto newsDto) {

        String string = mapArrayListToString(newsDto.getArticles());

        try (FileOutputStream fileOutputStream = new FileOutputStream(FILE_PATH);
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)) {

            objectOutputStream.writeObject(string);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String mapArrayListToString(List<NewsArticlesDto> list) {
        return list.stream().map(this::mapToString)
                .collect(Collectors.joining(System.lineSeparator()));
    }

    private String mapToString(NewsArticlesDto dto) {
        return dto.getTitle() + " : " + dto.getDescription() + " : " + dto.getAuthor();
    }

}
