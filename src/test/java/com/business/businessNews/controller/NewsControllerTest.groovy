package com.business.businessNews.controller


import com.business.businessNews.service.NewsService
import spock.lang.Specification

class NewsControllerTest extends Specification {

    NewsService newsService = Mock(NewsService)

    NewsController underTest = new NewsController(newsService)

    def "NewsController should return getNews method from NewsService"() {

        when:
        underTest.getNews()

        then:
        1 * newsService.getNews()
    }
}
